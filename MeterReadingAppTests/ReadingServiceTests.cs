﻿using AppCore;
using Data;
using FakeItEasy;
using MeterReadingApp.Controllers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualBasic;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MeterReadingAppTests
{
    [TestFixture]
    class ReadingServiceTests
    {

        private IReadingService _readingService;
        private IAccountService _accountService;
        private MeterReadingContext _meterReadingContext;

        [SetUp]
        public void Setup()
        {

            _meterReadingContext = new MeterReadingContext();
 
            TestDataDB db = new TestDataDB();
            db.CreateTestData(_meterReadingContext);

            _accountService =  A.Fake<IAccountService>();
            _readingService = new ReadingService(_accountService, _meterReadingContext);
        }

        [Test]
        public void GetReadingByAccountId_ReturnOK()
        {
            //Arrange  

            var accountId = 1234;

            //Act  
            var readings =  _readingService.GetReadingsByAccountId(accountId);

            //Assert  
            Assert.IsNotEmpty(readings);
        }

        [Test]
        public void GetReadingByAccountId_ReturnNull()
        {
            //Arrange  

            var accountId = 12345;

            //Act  
            var readings = _readingService.GetReadingsByAccountId(accountId);

            //Assert  
            Assert.IsEmpty(readings);
        }

        [Test]
        public void GetReadingByAccountId_NullData()
        {
            //Arrange  

            int? accountId = null;

            //Act  
            var readings = _readingService.GetReadingsByAccountId(accountId);

            //Assert  
            Assert.IsEmpty(readings);
        }

        [Test]
        public void GetReadingByAccountId_CompareResult()
        {
            //Arrange  

            int? accountId = 3456;
            List<Reading> expected = new List<Reading>();
                
            expected.Add(new Reading(){ AccountId = 3456, MeterReadValue = 8765, MeterReadingDateTime = DateAndTime.Now });

            //Act  
            var readings = _readingService.GetReadingsByAccountId(accountId);

            //Assert  
            Assert.IsNotEmpty(readings);
            Assert.AreEqual(expected.FirstOrDefault().AccountId, readings.FirstOrDefault().AccountId);
            Assert.AreEqual(expected.FirstOrDefault().MeterReadValue, readings.FirstOrDefault().MeterReadValue);
            Assert.AreEqual(expected.FirstOrDefault().MeterReadingDateTime.Date, readings.FirstOrDefault().MeterReadingDateTime.Date);
        }

        [Test]
        public void AddReading_ReturnTrue_ValidData()
        {
            //Arrange  

            Reading reading = new Reading() { ReadingId = 4, AccountId = 4321, MeterReadValue = 8910, MeterReadingDateTime = DateAndTime.Now };

            //Act  
            var result = _readingService.AddReading(reading);

            //Assert  
            Assert.IsTrue(result);
        }

        [Test]
        public void AddReading_ReturnFalse_InValidData()
        {
            //Arrange  

            Reading reading = new Reading() { AccountId = 1, MeterReadValue = 999999, MeterReadingDateTime = DateAndTime.Now };

            //Act  
            var result = _readingService.AddReading(reading);

            //Assert  
            Assert.IsFalse(result);
        }


        [Test]
        public void ValidateReading_ReturnTrue_ValidData()
        {
            //Arrange  

            Reading reading = new Reading() { AccountId = 1234, MeterReadValue = 12345, MeterReadingDateTime = DateAndTime.Now };

            //Act  
            var result = _readingService.ValidateReading(reading);

            //Assert  
            Assert.IsTrue(result);
        }

        [Test]
        public void ValidateReading_ReturnFalse_InvalidData()
        {
            //Arrange  

            Reading reading = new Reading() { AccountId = 1234, MeterReadValue = 999999, MeterReadingDateTime = DateAndTime.Now };

            //Act  
            var result = _readingService.ValidateReading(reading);

            //Assert  
            Assert.IsFalse(result);
        }


        [Test]
        public void DoesReadingExist_ReturnTrue_ReadAlreadyExists()
        {
            //Arrange  

            Reading reading = new Reading() { AccountId = 1234, MeterReadValue = 9887, MeterReadingDateTime = DateAndTime.Now };

            //Act  
            var result = _readingService.DoesReadingExist(reading);

            //Assert  
            Assert.IsTrue(result);
        }


        [Test]
        public void DoesReadingExist_ReturnFalse_ReadNotExists()
        {
            //Arrange  

            Reading reading = new Reading() { AccountId = 1234, MeterReadValue = 9889, MeterReadingDateTime = DateAndTime.Now };

            //Act  
            var result = _readingService.DoesReadingExist(reading);

            //Assert  
            Assert.IsFalse(result);
        }

        [Test]
        public void UpdateReading_ValidData()
        {
            //Arrange  

            Reading reading = new Reading() { ReadingId = 1, AccountId = 1234, MeterReadValue = 9999, MeterReadingDateTime = DateAndTime.Now };

            //Act  

            _readingService.UpdateReading(reading);

            var result = _readingService.GetReadingById(reading.ReadingId);

            //Assert  
            Assert.AreEqual(result.MeterReadValue, 9999);
        }

        [Test]
        public void DeleteReading_ValidData()
        {
            //Arrange  

            Reading reading = new Reading() { ReadingId = 2, AccountId = 1234, MeterReadValue = 6789, MeterReadingDateTime = DateAndTime.Now };

            //Act  

            _readingService.DeleteReading(reading);

            var result = _readingService.GetReadingById(reading.ReadingId);

            //Assert  
            Assert.IsNull(result);
        }

    }
}
