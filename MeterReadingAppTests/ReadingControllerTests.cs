﻿using AppCore;
using Data;
using FakeItEasy;
using MeterReadingApp.Controllers;
using Microsoft.AspNetCore.Mvc;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace MeterReadingAppTests
{
    [TestFixture]
    class ReadingControllerTests
    {

        private IReadingService _readingService;
        private IAccountService _accountService;
        private ICSVParseService _cSVParseService;
        private MeterReadingContext _meterReadingContext;


        [SetUp]
        public void Setup()
        {

            _meterReadingContext = new MeterReadingContext();

            TestDataDB db = new TestDataDB();
            db.CreateTestData(_meterReadingContext);

            _accountService = A.Fake<IAccountService>();
            _cSVParseService = A.Fake<ICSVParseService>();
            _readingService = A.Fake<IReadingService>();
        }

        [Test]  
        public void GetReadingDetailsById_Return_OkResult()
        {
            //Arrange  
            var controller = new ReadingsController(_meterReadingContext,_readingService,_cSVParseService);
            var readingId = 2;

            //Act  
            var data =  controller.Details(readingId);

            //Assert  
            Assert.IsInstanceOf<ViewResult>(data);
        }

        [Test]
        public void GetReadingDetailsById_Return_NotFound()
        {
            //Arrange  
            var controller = new ReadingsController(_meterReadingContext, _readingService, _cSVParseService);
            var readingId = 500;
            A.CallTo(() => _readingService.GetReadingById(readingId)).Returns(null);

            //Act  
            var data = controller.Details(readingId);

            //Assert  
            Assert.IsInstanceOf<NotFoundResult>(data);
        }

        [Test]
        public void GetReadingDetailsById_Return_BadRequest()
        {
            //Arrange  
            var controller = new ReadingsController(_meterReadingContext, _readingService, _cSVParseService);
            var readingId = -2;

            //Act  
            var data = controller.Details(readingId);

            //Assert  
            Assert.IsInstanceOf<BadRequestResult>(data);
        }

        [Test]
        public void SearchReadingsByAccountId_Return_OkResult()
        {
            //Arrange  
            var controller = new ReadingsController(_meterReadingContext, _readingService, _cSVParseService);
            var accountId = 2;

            //Act  
            var data = controller.Search(accountId);

            //Assert  
            Assert.IsInstanceOf<ViewResult>(data);
        }

        [Test]
        public void SearchReadingsByAccountId_Return_NotFound()
        {
            //Arrange  
            var controller = new ReadingsController(_meterReadingContext, _readingService, _cSVParseService);
            var accountId = 5;
            A.CallTo(() => _readingService.GetReadingsByAccountId(accountId)).Returns(null);

            //Act  
            var data = controller.Search(accountId);

            //Assert  
            Assert.IsInstanceOf<NotFoundResult>(data);
        }

        [Test]
        public void SearchReadingsByAccountId_Return_BadRequest()
        {
            //Arrange  
            var controller = new ReadingsController(_meterReadingContext, _readingService, _cSVParseService);
            var accountId = -2;

            //Act  
            var data = controller.Search(accountId);

            //Assert  
            Assert.IsInstanceOf<BadRequestResult>(data);
        }

        [Test]
        public void CreateReading_Return_OkResult()
        {
            //Arrange  
            var controller = new ReadingsController(_meterReadingContext, _readingService, _cSVParseService);
            var reading = new Reading() {AccountId = 2345, MeterReadValue = 678, MeterReadingDateTime = DateTime.Now };

            A.CallTo(() => _readingService.AddReading(reading)).Returns(true);

            //Act  
            var data = controller.Create(reading);

            //Assert  
            Assert.IsInstanceOf<RedirectToActionResult>(data);
        }

        [Test]
        public void CreateReading_Return_BadRequest()
        {
            //Arrange  
            var controller = new ReadingsController(_meterReadingContext, _readingService, _cSVParseService);
            var reading = new Reading() { AccountId = 2345, MeterReadValue = -678, MeterReadingDateTime = DateTime.Now };

            A.CallTo(() => _readingService.AddReading(reading)).Returns(false);

            //Act  
            var data = controller.Create(reading);

            //Assert  
            Assert.IsInstanceOf<BadRequestResult>(data);
        }

        [Test]
        public void UpdateReading_Return_OkResult()
        {
            //Arrange  
            var controller = new ReadingsController(_meterReadingContext, _readingService, _cSVParseService);
            var id = 1;
            var reading = new Reading() { ReadingId = 1, AccountId = 2345, MeterReadValue = 6789, MeterReadingDateTime = DateTime.Now };

            A.CallTo(() => _readingService.UpdateReading(reading));

            //Act  
            var data = controller.Edit(id, reading);

            //Assert  
            Assert.IsInstanceOf<RedirectToActionResult>(data);
        }

        [Test]
        public void UpdateReading_Return_BadRequest()
        {
            //Arrange  
            var controller = new ReadingsController(_meterReadingContext, _readingService, _cSVParseService);
            var id = 1;
            var reading = new Reading() { ReadingId = 1, AccountId = 2345, MeterReadValue = -6789, MeterReadingDateTime = DateTime.Now };

            A.CallTo(() => _readingService.UpdateReading(reading)).Throws<ArgumentException>();

            //Act  
            var data = controller.Edit(id, reading);

            //Assert  
            Assert.IsInstanceOf<BadRequestResult>(data);
        }

        [Test]
        public void UpdateReading_Return_NotFound()
        {
            //Arrange  
            var controller = new ReadingsController(_meterReadingContext, _readingService, _cSVParseService);
            var id = 15;
            var reading = new Reading() { AccountId = 2345, MeterReadValue = 6789, MeterReadingDateTime = DateTime.Now };

            A.CallTo(() => _readingService.UpdateReading(reading));

            //Act  
            var data = controller.Edit(id, reading);

            //Assert  
            Assert.IsInstanceOf<NotFoundResult>(data);
        }

        [Test]
        public void DeleteReading_Return_OkResult()
        {
            //Arrange  
            var controller = new ReadingsController(_meterReadingContext, _readingService, _cSVParseService);
            var reading = new Reading() { ReadingId = 1, AccountId = 2345, MeterReadValue = 6789, MeterReadingDateTime = DateTime.Now };

            A.CallTo(() => _readingService.DeleteReading(reading));

            //Act  
            var data = controller.DeleteConfirmed(reading.ReadingId);

            //Assert  
            Assert.IsInstanceOf<RedirectToActionResult>(data);
        }

    }
}
