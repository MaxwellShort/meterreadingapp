using Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualBasic;
using NUnit.Framework;

namespace MeterReadingAppTests
{
    public class TestDataDB
    {
        public TestDataDB()
        {
        }
        public void CreateTestData(MeterReadingContext context)
        {
            context.Database.EnsureDeleted();
            context.Database.EnsureCreated();

            context.Accounts.AddRange(
                new Account() { AccountId = 1234, FirstName = "Jane", LastName = "Test" },
                new Account() { AccountId = 4321, FirstName = "Dave", LastName = "Test" }

            );

            context.Readings.AddRange(
                new Reading() { AccountId = 1234, MeterReadValue = 9887, MeterReadingDateTime = DateAndTime.Now},
                new Reading() { AccountId = 1234, MeterReadValue = 6789, MeterReadingDateTime = DateAndTime.Now},
                new Reading() { AccountId = 3456, MeterReadValue = 8765, MeterReadingDateTime = DateAndTime.Now}
            );
            context.SaveChanges();


            foreach (var entity in context.ChangeTracker.Entries()) //Fixes Issue "The instance of entity type 'Reading' cannot be tracked...
            {
                entity.State = EntityState.Detached;
            }
        }
    }
}