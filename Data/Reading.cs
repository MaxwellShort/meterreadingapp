﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace Data
{
    public class Reading
    {
        public Reading()
        {
        }

        public Reading(int accountId, DateTime readingDateTime, int readingValue)
        {
            this.AccountId = accountId;
            this.MeterReadingDateTime = readingDateTime;
            this.MeterReadValue = readingValue;
        }

  
        public int ReadingId { get; set; }
        public int AccountId { get; set; }
        public DateTime MeterReadingDateTime { get; set; }
        public int MeterReadValue { get; set; }


    }
}
