﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data
{
    public class Result
    {

        public int Total { get; set; }
        public int Success { get; set; }
        public int Failure { get; set; }


    }
}
