﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Data
{
    public class MeterReadingContext : DbContext
    {
        public MeterReadingContext()
        { 
        }

        public MeterReadingContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Account> Accounts {get; set;}
        public DbSet<Reading> Readings {get; set;}

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {

            // Add to Config File
            optionsBuilder.UseSqlite("Filename = MeterReadingDataBase.db");
        }
    }
}
