﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data
{
    public class Account
    {
        public Account()
        {
        }

        public Account(string firstName, string lastName)
        {
            this.FirstName = firstName;
            this.LastName = lastName;
        }

        public int AccountId { get; set; }
        public String FirstName { get; set; }
        public String LastName { get; set; }


    }
}
