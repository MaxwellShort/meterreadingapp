﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Data;
using AppCore;
using Microsoft.AspNetCore.Http;
using Castle.Core.Internal;

namespace MeterReadingApp.Controllers
{
    public class ReadingsController : Controller
    {
        private readonly MeterReadingContext _context;
        private readonly IReadingService _readingService;
        private readonly ICSVParseService _CSVParseService;

        public ReadingsController(MeterReadingContext context, IReadingService readingService, ICSVParseService CSVParseService)
        {
            _context = context;
            _readingService = readingService;
            _CSVParseService = CSVParseService;
        }

        // GET: Readings
        public IActionResult Index()
        {
            return View();
        }

        //GET: Search
        public IActionResult Search(int? id)
        {
            if (id == null || id <= 0)
            {
                return BadRequest();
            }

            var account = _readingService.GetReadingsByAccountId(id);

            if (account == null)
                {
                return NotFound();
            }
            else
            {
                return View(account);
            }
        }

        // GET: Readings/Details/5
        public IActionResult Details(int? id)
        {
            if (id == null || id <= 0)
            {
                return BadRequest();
            }

            var reading = _readingService.GetReadingById(id);

            if (reading == null)
            {
                return NotFound();
            }

            return View(reading);
        }

        // GET: Readings/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Readings/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create([Bind("ReadingId,AccountId,ReadingDateTime,ReadingValue")] Reading reading)
        {


                if (_readingService.AddReading(reading))
                {

                return RedirectToAction("Details", new { id = reading.ReadingId });

                }
                else
                {
                    return BadRequest();
                }
            

            
        }

        public IActionResult UploadReadings()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ImportReadingsFile(IFormFile importFile)
        {
            if (importFile == null) return Json(new { Status = 0, Message = "No File Selected" });

            try
            {
                var readings = _CSVParseService.ReadingParser(importFile.OpenReadStream()); 

                var results = _readingService.AddReadings(readings);


                return Json(new
                {
                    Status = 1,
                    Message = "File Imported Successfully." + "</br>" +
                    "Total Reads:" + results.Total + "</br>" +
                    "Successful Reads:" + results.Success + "</br>" +
                    "Failed Reads:" + results.Failure
                });


            }
            catch (Exception ex)
            {
                return Json(new { Status = 0, Message = ex.Message });
            }
        }

        // GET: Readings/Edit/{id}
        public IActionResult Edit(int? id)
        {
            if (id == null || id <= 0)
            {
                return BadRequest();
            }

            var reading = _readingService.GetReadingById(id);

            if (reading == null)
            {
                return NotFound();
            }
            return View(reading);
        }

        // POST: Readings/Edit/{id}
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(int id, [Bind("ReadingId,AccountId,ReadingDateTime,ReadingValue")] Reading reading)
        {
            if (id != reading.ReadingId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _readingService.UpdateReading(reading);
                }
                catch
                {
                        return BadRequest();
                }
                return RedirectToAction("Search", new { id = reading.AccountId });
            }
            return View(reading);
        }

        // GET: Readings/Delete/{id}
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var reading = _readingService.GetReadingById(id);

            if (reading == null)
            {
                return NotFound();
            }

            return View(reading);
        }

        // POST: Readings/Delete/{id}
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            
            var reading = _readingService.GetReadingById(id);
            try
            {
                _readingService.DeleteReading(reading);

                return RedirectToAction("Search", new { id = reading.AccountId });
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }


    }
}
