﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Data;
using AppCore;

namespace MeterReadingApp.Controllers
{
    public class AccountsController : Controller
    {
        private readonly MeterReadingContext _context;
        private readonly IAccountService _accountService;

        public AccountsController(MeterReadingContext context, IAccountService accountService)
        {
            _context = context;
            _accountService = accountService;
        }

        // GET: Accounts
        public IActionResult Index()
        {

            return View( _accountService.GetAccounts());
        }


        // GET: Accounts/Details/{id}
        public IActionResult Details(int? id)
        {
            if (id == null || id <= 0)
            {
                return BadRequest();
            }

            var account = _accountService.GetAccountById(id);

            if (account == null)
            {
                return NotFound();
            }

            return View(account);
        }

        // GET: Accounts/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Accounts/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create([Bind("AccountId,FirstName,LastName")] Account account)
        {
            try
            {
                if (ModelState.IsValid)
                {

                    _accountService.AddAccount(account);
                    return RedirectToAction(nameof(Index));
                }
                else
                {
                    return View(account);
                }
            }
            catch
            {
                return BadRequest();
            }
        }

        // GET: Accounts/Edit/{id}
        public IActionResult Edit(int? id)
        {
            if (id == null || id <= 0)
            {
                return BadRequest();
            }

            var account = _accountService.GetAccountById(id);

            if (account == null)
            {
                return NotFound();
            }

            return View(account);
        }

        // POST: Accounts/Edit/{id}
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(int id, [Bind("AccountId,FirstName,LastName")] Account account)
        {
            if (id != account.AccountId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _accountService.UpdateAccount(account);
                }
                catch
                {
                    if (!_accountService.DoesAccountExist(account.AccountId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(account);
        }

        // GET: Accounts/Delete/{id}
        public IActionResult Delete(int? id)
        {
            if (id == null || id <= 0)
            {
                return BadRequest();
            }

            var account = _accountService.GetAccountById(id);

            if (account == null)
            {
                return NotFound();
            }

            return View(account);
        }

        // POST: Accounts/Delete/{id}
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            var account = _accountService.GetAccountById(id);
            _accountService.DeleteAccount(account);
            return RedirectToAction(nameof(Index));
        }

    }
}
