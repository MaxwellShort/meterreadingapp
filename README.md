#Meter Reading App  
-This is an App to Read Meter Reading CSV Files, Parse Each Line and Add them to a Database.  

#Usage  
-An account needs to exist for a reading for it to be succesfully added. Ensure account has been created using the accounts section before adding any reads.  
-Accounts are viewable on the Accounts page.  
-Reading page allows for all readings for an account ID to be searched.  
-Single Readings can be created using the create new page.   
-CSVs can be loaded using the upload new file page.  
-Reads can not be added multiple times to the Database. Checks for matching read value & read date.  


#Improvements  
	-Add validation on the CSV columns when read rather than validate after the fact. This would allow different format dates to be used and not cause issues. Also Add rules for Nulls/Whitespaces.  
	-Add testing for AccountsController and AccountService.  
	-Return 200 Ok on successful controller requests rather than 302.  
	-Display failure reason for any rows that are not loaded from CSV.  
	-Improve unit test creation.  
	-Add Comments.  
	-Swap out ExcelDataReader for a more simple CSV reading library.  
	-Add functionality to load in Accounts using CSV reader.  
	-Improve UI.

#Contact  
-Maxwell Short  
-Maxwell.Short@ensek.co.uk  






