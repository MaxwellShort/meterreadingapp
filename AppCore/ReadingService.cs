﻿using Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace AppCore
{
    public class ReadingService : IReadingService
    {

        private readonly IAccountService _accountService;
        private readonly MeterReadingContext _meterReadingContext;

        public ReadingService(IAccountService accountService, MeterReadingContext meterReadingContext)
        {
            _accountService = accountService;
            _meterReadingContext = meterReadingContext;
        }

        public List<Reading> GetReadingsByAccountId(int? accountid)
        {

            List<Reading> result;

            var context = _meterReadingContext;  //make this global? will mean less repetition and easier to change if context changes, but then again, you could just change the entire layer
            result = context.Readings.Where(x => x.AccountId == accountid).ToList();

            return result;
        }

        public Reading GetReadingById(int? readingId)
        {
            var context = _meterReadingContext;
            var result = context.Readings.FirstOrDefault(x => x.ReadingId == readingId);
            return result;
        }

        public bool AddReading(Reading reading) //change the return to something better
        {

            var context = _meterReadingContext;
            var accountId = _accountService.GetAccountById(reading.AccountId);
            if (accountId != null && ValidateReading(reading))
            {
                context.AddRange(reading);
                context.SaveChanges();
                return true;
            }

            else
            {
                return false;
            }
        }

        public Result AddReadings(List<Reading> readings) //change the array to something better, probably a class
        {
            Result result = new Result();

            result.Total = readings.Count();

            foreach (Reading reading in readings)
            {
                if (AddReading(reading))
                { 
                result.Success++;
                }
                
            }

            result.Failure = result.Total - result.Success;

            return result;

        }

        public bool ValidateReading(Reading reading)
        {

            if (reading.MeterReadingDateTime > DateTime.Now)
            {
                return false;
            }
            else if (!int.TryParse(reading.MeterReadValue.ToString(), out int result))
            {
                return false;
            }
            else if (reading.MeterReadValue > 99999 || reading.MeterReadValue < 0)
            {
                return false;
            }
            else if (DoesReadingExist(reading))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public bool DoesReadingExist(Reading reading)
        {

            var context = _meterReadingContext;

            var result = context.Readings.Any(x => x.MeterReadingDateTime.Date == reading.MeterReadingDateTime.Date && x.MeterReadValue == reading.MeterReadValue);

            return result;

        }

        public void UpdateReading(Reading reading)
        {

            if (ValidateReading(reading))
            {
                var context = _meterReadingContext;

                context.Readings.Update(reading);
                context.SaveChanges();
            }
            else
            {
                throw new ArgumentException("New Reading Not Valid");
            }
        }

        public void DeleteReading(Reading reading)
        {
            var context = _meterReadingContext;

                context.Readings.Remove(reading);
                context.SaveChanges();

        }



    }
  
}
