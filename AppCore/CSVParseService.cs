﻿using CsvHelper;
using CsvHelper.Configuration;
using CsvHelper.TypeConversion;
using Data;
using ExcelDataReader;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;

namespace AppCore
{
    public class CSVParseService : ICSVParseService 
    {
        public List<Reading> ReadingParser(Stream stream)
        {
            var emptylist = new List<Reading>();

            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);

            try
            {
                using (var reader = ExcelReaderFactory.CreateCsvReader(stream, new ExcelReaderConfiguration() { FallbackEncoding = Encoding.GetEncoding(1252) }))
                {

                    var dataSet = reader.AsDataSet(new ExcelDataSetConfiguration
                    {
                        ConfigureDataTable = _ => new ExcelDataTableConfiguration
                        {
                            UseHeaderRow = true // To set First Row As Column Names  
                        }
                    });

                    if (dataSet.Tables.Count > 0)
                    {
                        var dataTable = dataSet.Tables[0];
                        foreach (DataRow objDataRow in dataTable.Rows)
                        {
                            if (objDataRow.ItemArray.Length > 0  && Int32.TryParse((objDataRow["MeterReadValue"]).ToString(), out int result))
                            {
                                emptylist.Add(new Reading()
                                {
                                    AccountId = Convert.ToInt32(objDataRow["AccountId"]),
                                    MeterReadingDateTime = Convert.ToDateTime(objDataRow["MeterReadingDateTime"].ToString()),
                                    MeterReadValue = Convert.ToInt32(objDataRow["MeterReadValue"])
                                        
                                });
                            }
                        }
                    }

                }
            }
            catch (Exception)
            {
                throw;
            }

            return emptylist;
        }

    }
}
