﻿using Data;
using System.Collections.Generic;

namespace AppCore
{
    public interface IAccountService
    {
        List<Account> GetAccounts();

        Account GetAccountById(int? accountId);

        bool AddAccount(Account account);

        bool ValidateAccount(Account account);

        bool DoesAccountExist(int accountId);

        void UpdateAccount(Account account);

        void DeleteAccount(Account account);
    }
}