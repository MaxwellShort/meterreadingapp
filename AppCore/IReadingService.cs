﻿using Data;
using System.Collections.Generic;

namespace AppCore
{
    public interface IReadingService
    {
        List<Reading> GetReadingsByAccountId(int? accountid);

        Reading GetReadingById(int? readingId);

        bool AddReading(Reading reading);

        Result AddReadings(List<Reading> readings);

        bool ValidateReading(Reading reading);

        bool DoesReadingExist(Reading reading);

        void UpdateReading(Reading reading);

        void DeleteReading(Reading reading);
    }
}