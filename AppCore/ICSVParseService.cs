﻿using Data;
using System.Collections.Generic;
using System.IO;

namespace AppCore
{
    public interface ICSVParseService
    {
        List<Reading> ReadingParser(Stream stream);

    }
}