﻿using Castle.Core.Internal;
using Data;
using Microsoft.EntityFrameworkCore.Internal;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AppCore
{
    public class AccountService : IAccountService
    {
        private readonly MeterReadingContext _meterReadingContext;

        public AccountService(MeterReadingContext meterReading)
        {
            _meterReadingContext = meterReading;
        }

        public List<Account> GetAccounts()
        {
            List<Account> result;

            var context = _meterReadingContext;

            result = context.Accounts.ToList();

            return result;
        }

        public Account GetAccountById(int? accountId)
        {
            var context = _meterReadingContext;

            var result = context.Accounts.FirstOrDefault(x => x.AccountId == accountId);  
            
            return result;
            
        }

        public bool AddAccount(Account account) //Is there a better way to return success?
        {

            var context = _meterReadingContext;

            if (account != null  && ValidateAccount(account))
            {
                context.AddRange(account);
                context.SaveChanges();
                return true;
            }

            else
            {
                return false;
            }
        }

        public bool ValidateAccount(Account account) //There has to be a better way to do this
        {

            if (account.FirstName.IsNullOrEmpty())
            {
                return false;
            }
            else if (account.LastName.IsNullOrEmpty())
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public bool DoesAccountExist(int accountId)
        {
            return _meterReadingContext.Accounts.Any(x => x.AccountId == accountId);

        }


        public void UpdateAccount(Account account)
        {

            if (ValidateAccount(account))
            {
                var context = _meterReadingContext;

                context.Accounts.Update(account);
                context.SaveChanges();
            }

        }

        public void DeleteAccount(Account account)
        {
            var context = _meterReadingContext;

            context.Accounts.Update(account);
            context.SaveChanges();

        }


    }
}
